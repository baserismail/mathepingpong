""" 
Make a List 1 to 100, named myListe
"""

myListe=[*range(1,101)]

index=0

for zahl in myListe:
    message=""
    if int(zahl)%3==0:
       message+="Ping"

    if int(zahl)%5==0:
        message+="Pong"
    if int(zahl)%7==0:    #zusätzlich bei 7 teilbar "Plock"
        message+="Plock"
    if "3" in str(zahl) or "5" in str(zahl):
        if "3" in str(zahl):
            message+="Ping"      # Wenn die Zahl eine 3 enthält "Ping"
        if "5" in str(zahl):     # Wenn die Zahl eine 5 enthält "Pong"
            message+="Pong"
        
 
    if message=="":
        message=zahl
    
    myListe[index]=message   
   
    index+=1


print(myListe)
